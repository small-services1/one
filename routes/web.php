<?php

use Illuminate\Support\Facades\Route;
use App\MobileDetect\Mobile_Detect;
// use App\Http\Controllers\AjaxController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/tst', function () {
	$detect = new Mobile_Detect;

	$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
	if($deviceType == 'computer') {
		return 'computer';
	} elseif ($deviceType == 'tablet') {
		return 'tablet';
	} elseif ($deviceType == 'phone') {
		return 'phone';
	}
    // return view('test');
});

Route::post('/api', function () {
	if (Request::ajax()) {
		$response = Request::post('data_array');
		$result = $response['number'] * ($response['percent'] / 100);
		// $test = 'Hello';
	 	//    return $test;
		return $result;
		// return var_dump(Response::json(Request::all('data_array')));
	} else return 'Request wasn\'t received';
});
// pcfn = percent from number
// Route::post('/api', 'AjaxController@index');






Route::get('/show_session', function() {
    $data = session()->all();
    return dd($data);
});
Route::get('/phpinfo', function() {
    return phpinfo();
});
Route::get('/clear_cache', function () {
    Log::debug('CLEARED');
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    return 'Cache was cleared.';
});