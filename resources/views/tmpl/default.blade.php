@include('elements.header')
@include('elements.head')
<div class="main-container">
	<div class="container-fluid">
		<div class="row">
			<div class="col col-np col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<div class="content-block left-block">
					@include('elements.left')

				</div>
			</div>
			<div class="col col-np col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="content-block center-block">
					@yield('content')
				</div>
			</div>
			<div class="col col-np col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<div class="content-block right-block">
					@include('elements.right')

				</div>
			</div>
		</div>
	</div>
</div>
@include('elements.foot')
@include('elements.footer')