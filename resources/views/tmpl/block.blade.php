@include('elements.header')
<div class="custom-container">
	<section id="content">
		@include('elements.head')
		@yield('content')
	</section>
</div>
@include('elements.footer')