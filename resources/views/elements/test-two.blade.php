<div class="content area">
    <h1>@yield('h1')</h1>
	<div class="container">
		<div class="col-12 col-sm-12 col-md-6 col-lg-6">
			<div class="main-content">
				<div class="input-block">
					<input type="number" name="percent" placeholder="Количество">
					<p>% от</p>
					<input type="number" name="number" placeholder="числа">
				</div>
				<div id="percent" class="full-accept btn btn-info">Вычислить</div>
				<div class="result"></div>
			</div>
		</div>
	</div>
</div>