// console.log(JSON.parse($('input[name="tkQsf12a"]').val()));

$(document).ready(function(){
    var sh = $(document).height();
    // Блок установки размеров рабочей области (основная область)
    if (sh < $('.area').height()) $('#content').css('padding-bottom', '40px');
    var HDpadding = parseInt($('.nav-opt').css('padding-top'),10)+parseInt($('.nav-opt').css('padding-bottom'),10);
    var HDmargin = parseInt($('.nav-opt').css('margin-top'),10)+parseInt($('.nav-opt').css('margin-bottom'),10);
    var hb = ($('.nav-opt').height()+HDmargin+HDpadding);
    // console.log(hb);
    let h;
    h = sh-hb-20;
    // h = $(document).height()-h;
    $('#content').css('min-height', sh+'px');
    $('.area').css('min-height', h+'px');
    

    $(function(){
        // Изменение размеров рабочей области (основная область)
        var resTO=false;
        $(window).on('orientationchange', function() {
            clearTimeout(resTO);
            resTO = setTimeout(function () {
                sh = $(window).height();
                console.log(sh);
                let h;
                h = sh-hb-20;
                // console.log( $(document).height() );
                $('#content').css('min-height', sh+'px');
                $('.area').css('min-height', h+'px');
                if (sh < $('.area').height()) $('#content').css('padding-bottom', '40px');
            },200)
        })
    })

    // Получить процент от числа
    $('#percent').on('click', function(){
    	let percent = $('input[name="percent"]').val();
    	let number = $('input[name="number"]').val();
    	let array = new Object;
    	array['percent'] = percent;
    	array['number'] = number;
    	$.post("/api", {"data_array": array}).done(function (data) {
            $('.result').html('<p><span class="accentuated">Результат:</span> '+array['percent']+'<span class="elusive">% от</span> '+array['number']+' <span class="elusive">=</span> <span class="accentuated">'+data+'</span></p>');
            // console.log(data);
        });
    })
})